https://www.doomworld.com/forum/topic/98682-doomloader-for-unity/

**Load Doom wads directly into Unity.**
(Currently Doom1 shareware wad)

 

Builds meshes for linedefs and sectors.
Constructs textures directly from WAD.
Creates Unity gameobjects for things.
Has option to override materials and textures.

 

Open up the test_scene.scene and hit play to load the first map
Change the "Autoload Map" parameter in the WadLoader component to load different map "E1M1" "E1M2" "E1M3"...
You can walk around using arrow keys and open doors with spacebar.
(currently only doortype 1 and it's keycard variants are done)

 

 

Asset store package is currently pending for approval, but you can download it here.

Just create a new project and extract the zip contents into /assets

The zip package contains Doom1 shareware wad. 